package com.hendisantika.itext;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootItextApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootItextApplication.class, args);
	}
}
