package com.hendisantika.itext.service;

import com.hendisantika.itext.domain.Car;

import java.util.List;

/**
 * Created by hendisantika on 7/13/17.
 */
public interface ICarService {
    public List<Car> findAll();
}
